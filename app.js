var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');

var app = express();

app.use(express.static(path.join(__dirname, "public")));

app.use(bodyParser.urlencoded({extended:true}));

app.get('/', function(req, res){
  res.sendfile("index.html");
});

const pdf = require("./routes/pdfmake");
app.use("/pdfMake", pdf);


app.listen(3010, function(){
  console.log('Server is running on http://localhost:{port}'.replace("{port}", 3010));
})
