

const express = require('express');
const router = express.Router();
const fs = require('fs');
const ejs = require('ejs');

const pdfMake = require('html-pdf');

const date = new Date();
var day = date.getDate();
if (day < 10){
  day = "0" + day;
}
var month = date.getMonth() + 1;
if (month < 10){
  month = "0" + month;
}
const year = date.getFullYear();
var hour = date.getHours();
if (hour < 10){
  hour = "0" + hour;
}
var min = date.getMinutes();
if (min < 10){
  min = "0" + min;
}
const allDate = String(hour) + ":" + String(min) + " " + String(day) + "." + String(month) + "." + String(year);




router.post('/pdf', function(req, res, next){

  const fname = req.body.fname;
  const lname = req.body.lname;
  const occupation = req.body.occupation;
  const bugReport = req.body.bugReport;
  const params = {firstname : fname, lastname: lname, occupation: occupation, date: allDate, bugReport: bugReport};
  ejs.renderFile(__dirname + '/template.ejs', params, function(err, html) {
    const options = { format: 'A4', orientation: 'portrait'};
      const pdfDoc = pdfMake.create(html, options).toBuffer(function(err, buffer){
        if (err){
          res.end("Error");
          console.log(err);
        }
        console.log(allDate);

        res.writeHead(200, {
          "Content-type": "application/pdf",
          "Content-Disposition": 'attachment;filename="BugReport_{date}.pdf"'.replace("{date}", allDate)
        });
        res.end(buffer);
});

});

});

module.exports = router;
